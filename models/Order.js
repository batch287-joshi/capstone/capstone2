const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [ true, "User Id is required." ]
	},
	totalAmount: {
		type: Number
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	products : [
		{
			productId : {
				type: String,
				required: [ true, "Product Id is required."]
			},

			quantity : {
				type: Number,
				required: [ true, "Quantity is required."]
			}
		}
	]
})

module.exports = mongoose.model("Order", orderSchema);