const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// // Routes for checking if the user's email already exist in the database
// router.post("/checkEmail", (req,res) => {
// 	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
// });

// Route for user registration
router.post("/register", (req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication (login)
router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving user details
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
router.get("/:userId/userDetails", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	//const userData = auth.decode(req.headers.authorization);
	 // Will return the token.payload


	// Provides the user's ID for the getProfile controller method
	userController.getProfile(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for enroll user to a course
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	};

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});

router.post("/checkout", auth.verify, (req, res) => {

	const data = {
		products: req.body.products,
		userId: auth.decode(req.headers.authorization).id,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	userController.addOrder(data).then(resultFromController => res.send(resultFromController));
});

module.exports = router;