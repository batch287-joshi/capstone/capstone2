const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

// Route for creating a product
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});

// // Route for retrieving all the products
// router.get("/all", (req, res) => {

// 	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
// });

// Route for retrieving all the ACTIVE products
router.get("/active-products", (req, res) => {

	productController.activeProducts().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving a specific product
router.get("/:productId", (req, res) => {

	//console.log(req.params.courseId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	};

	productController.updateProduct(req.params, data).then(resultFromController => res.send(resultFromController));
});

// Route for archiving a product
router.patch("/:productId", auth.verify, (req, res) => {

	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

module.exports = router;