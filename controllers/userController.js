const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// module.exports.checkEmailExists = (reqBody) => {
	
// 	return User.find({ email: reqBody.email }).then(result => {
		
// 		// The "find" method returns a record if a match is found
// 		if(result.length > 0){
			
// 			return true
		
// 		// No duplicate email found
// 		// The user is not yet registered in the database
// 		} else {
			
// 			return false
		
// 		}
// 	})
// }

module.exports.registerUser = (reqBody) => {

	console.log(reqBody);

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false
		} else {
			return true
		}
	})
}

module.exports.loginUser = (reqBody) => {

	// .findOne() will look for the first document that matches
	return User.findOne({ email: reqBody.email }).then(result => {

		console.log(result);

		// Email doesn't exist
		if(result == null){

			return false;

		} else {

			// We now know that the email is correct, is password correct also?
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			console.log(isPasswordCorrect);
			console.log(result);

			// Correct password
			if(isPasswordCorrect){

				return { access: auth.createAccessToken(result) }

			// Password incorrect	
			} else {

				return false
				
			};

		};
	});
};

module.exports.getProfile = (reqParams) => {

	 // Will return the key/value of pair of userId: data.Id

	// Changes the value of the user's password to an empty string when returned to the frontend
	// Not doing so will expose the user's password which will also not be needed in other parts of our application
	// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application

	return User.findById(reqParams.userId).then(result => {
		result.password = "";
		// Returns the user information with the password as an empty string
		return result;
	})
}

module.exports.enroll = async (data) => {

	// 1st Await Update first the User Models
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.enrollments.push({ courseId: data.courseId });

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => { course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isCourseUpdated){
		return true;
	} else {
		return false;
	};
};

module.exports.addOrder = async (data) => {

	console.log(data);

	if(!data.isAdmin){

		let dataProducts = data.products

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let totalAm = 0;

		for(let i = 0; i < dataProducts.length; i++){
			let productPrice = await Product.findById(dataProducts[i].productId).then(result => {
				return result.price;
			})
			totalAm += dataProducts[i].quantity*productPrice;
		}
		let newOrder = new Order({
			userId: data.userId,
			products: data.products,
			totalAmount: totalAm
			// name: data.product.name,
			// description: data.product.description,
			// price: data.product.price
		});

		console.log(newOrder);
		console.log(totalAm);
		// Saves the created object to our database
		return newOrder.save().then((product, error) => {

			// Product creation failed
			if(error){

				return false;

			// Product creation successful
			} else {

				return true;
			}
		})
	}
}